# Return_of_the_MAC

## Description
This will allow you to easily chnage your MAC address using your Linux distro of choice. 

NOTE: This uses the ip command instead of ifconfig (as it is deprecated).

## Installation
Simply add the Return_of_the_MAC.py file to your directory of choice. /opt is a popular directory for this.

## Usage
Python3 /path/to/Return_of_the_MAC.py -i YOURCHOSENINTERFACE -m YOURCHOSENMACADDRESS


-i & --interface both let you select your interface


-m & --mac both let you select your new chosen MAC address

Example:

Python3 /path/to/Return_of_the_MAC.py -i eth0 -m 00:11:22:33:44:55

or

Python3 /path/to/Return_of_the_MAC.py --interface eth0 --mac 00:11:22:33:44:55

## Support
Please submit an issue if you run into any problems. 

## Roadmap
I will continue to update this for future python version releases.

## Contributing
If there are any optimizations that can help improve this tool, please let us know :)

## Authors and acknowledgment
v0odoo

## License
This tool is licensed under GPL3 

https://gitlab.com/v0odoolabs/return_of_the_mac/-/blob/main/LICENSE @ https://gitlab.com/v0odoolabs

## Project status
Maintained
