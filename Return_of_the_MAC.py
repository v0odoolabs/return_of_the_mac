#!/usr/bin/env python3
# Licensed under GPL3
# MAC address change tool

import subprocess
import optparse
import re


def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option("-i", "--interface", dest="target_interface", help="Set the target interface")
    parser.add_option("-m", "--mac", dest="change_mac", help="Set the new MAC address")
    (options, arguments) = parser.parse_args()
    if not options.target_interface:
        parser.error("[-] Please specify your target interface. Use --help for more information.")
    elif not options.change_mac:
        parser.error("[-] Please specify a MAC address. Use --help for more information.")
    return options


def mac_changer(target_interface, change_mac):
    print("[+} Changing MAC address for " + target_interface + " to " + change_mac)
    subprocess.call(["ip", "link", "set", "dev", target_interface, "down"])
    subprocess.call(["ip", "link", "set", "dev", target_interface, "address", change_mac])
    subprocess.call(["ip", "link", "set", "dev", target_interface, "up"])

def current_mac(target_interface):
    ip_result = subprocess.check_output(["ip", "a", "show", "dev", target_interface])
    return_mac_address = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", str(ip_result))

    if return_mac_address:
        return return_mac_address.group(0)
    else:
        print("[-] Where is the MAC?")


options = get_arguments()

the_current_mac = current_mac(options.target_interface)
print("The MAC is leaving: " + str(the_current_mac))

mac_changer(options.target_interface, options.change_mac)

the_current_mac = current_mac(options.target_interface)
if the_current_mac == options.change_mac:
    print("[+] The MAC has returned! " + the_current_mac)
else:
    print("[-] Uh oh.. The MAC did not return...")
